// Setup express dependency
const express = require("express");
// Create a Router instance
const router = express.Router();
// Import TaskController.js
const taskController = require("../controllers/taskController");

// Routes

// Route to get all tasks
router.get("/", (req, res) => {
	taskController.getAllTasks().then(resultFromController => res.send(`Tasks listed:	${resultFromController}`));
})

// Route to create a new task
router.post("/", (req, res) => {
	taskController.createTask(req.body).then(resultFromController => res.send(`Task created: ${resultFromController}`));
})

// Route for deleting a task
// localhost:3001/tasks/id
router.delete("/:id", (req, res) => {
	taskController.deleteTask(req.params.id).then(resultFromController => res.send(`Task deleted: ${resultFromController}`));
})

// Route for updating a task
router.put("/:id", (req, res) => {
	taskController.updateTask(req.params.id, req.body).then(resultFromController => res.send(`Task updated: ${resultFromController}`));
})

// ============= START OF ACTIVITY =============

// Route for getting a specific task
router.get("/:id", (req, res) => {
	taskController.getTask(req.params.id).then(resultFromController => res.send(`Task found: ${resultFromController}`));
})

// Route for changing task status
router.put("/:id/complete", (req, res) => {
taskController.completeTask(req.params.id, req.body).then(resultFromController => res.send(`Task completed: ${resultFromController}`));
})

// ============== END OF ACTIVITY ==============

module.exports = router;